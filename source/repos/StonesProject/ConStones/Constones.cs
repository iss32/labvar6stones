﻿using System;
using Enities;
using Logic;

namespace ConStones
{
    class ConStones
    {
        static void Main(string[] args)
        {
            StoneFactory factory = new StoneFactory();
            Necklace necklace = factory.CreateNecklace();
            NecklacePrinter necklacePrinter = new NecklacePrinter();
            necklacePrinter.PrintNecklace(necklace);
        }
    }
}
