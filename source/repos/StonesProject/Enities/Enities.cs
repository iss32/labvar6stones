﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enities
{
    abstract public class AbstractStone
    {
        public abstract string checkClass();
        private string _name;
        private double _weight;
        private double _cost;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public double Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }
    }

    abstract public class AbstractJewelStone : AbstractStone
    {
        private double _transparency;

        public double Transparency
        {
            get { return _transparency; }
            set { _transparency = value; }
        }
    }

    public class Diamond : AbstractJewelStone
    {
        public override string checkClass()
        {
            return "Diamond"; 
        }

        private string _colour;

        public string Colour
        {
            get { return _colour; }
            set { _colour = value; }
        }
    }

    public class Emerald : AbstractJewelStone
    {
        private int _facetsAmount;

        public override string checkClass()
        {
            return "Emerald";
        }

        public int FacetsAmount
        {
            get { return _facetsAmount; }
            set { _facetsAmount = value; }
        }
    }

    public class Ruby : AbstractJewelStone
    {
        private double _hardness;

        public override string checkClass()
        {
            return "Ruby";
        }

        public double Hardness
        {
            get { return _hardness; }
            set { _hardness = value; }
        }
    }

    ///.............................///

    public abstract class AbstractSemiJewelStone : AbstractStone
    {
        private double _saturation;

        public double Saturation
        {
            get { return _saturation; }
            set { _saturation = value; }
        }
    }

    public class Aquamarine : AbstractSemiJewelStone
    {
        private double _density;

        public override string checkClass()
        {
            return "Aquamarine";
        }

        public double Density
        {
            get { return _density; }
            set { _density = value; }
        }
    }

    public class Moonstone : AbstractSemiJewelStone
    {
        private double _shine;

        public override string checkClass()
        {
            return "Moonstone";
        }

        public double Shine
        {
            get { return _shine; }
            set { _shine = value; }
        }
    }

    public class Tourmaline : AbstractSemiJewelStone
    {
        private string _shape;

        public override string checkClass()
        {
            return "Tourmaline";
        }

        public string Shape
        {
            get { return _shape; }
            set { _shape = value; }
        }
    }

    public class Necklace
    {
        private List<AbstractStone> _necklace = new List<AbstractStone>();

        public List<AbstractStone> NecklaceItself
        {
            get { return _necklace; }
            set { _necklace = value; }
        }

        public void AddToNecklace(AbstractStone stone)
        {
            _necklace.Add(stone);
        }

        public void DeleteFromNecklace(AbstractStone stone)
        {
            _necklace.Remove(stone);
        }
    }
}