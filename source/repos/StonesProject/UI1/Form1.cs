﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enities;
using Logic;

namespace UI1
{
    public partial class Form1 : Form
    {
        private Necklace necklace;
        private void forCalc()
        {
            listBox1.Items.Clear();
            foreach (AbstractStone stone in necklace.NecklaceItself)
            {
                listBox1.Items.Add(stone.Name + " - " + stone.Cost.ToString() + " $");
            }
            PriceCalculator calculatorP = new PriceCalculator();
            WeightCalculator calculatorW = new WeightCalculator();
            label2.Text = "Стоимость: " + calculatorP.GetTotalPrice(necklace).ToString() + " $";
            label3.Text = "Масса: " + calculatorW.GetTotalWeight(necklace).ToString() + " карат";
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            StoneFactory necklaceFactory = new StoneFactory();
            necklace = necklaceFactory.CreateNecklace();
            forCalc();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
            {
                label3.Text = "Выберите камень из списка!";
            }
            else
            {
                label3.Text = "";
                necklace.NecklaceItself.RemoveAt(listBox1.SelectedIndex);
                forCalc();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
            {
                label3.Text = "Выберите камень из списка!";
            }
            else
            {
                label3.Text = "";
                Form2 temp = new Form2(necklace.NecklaceItself[listBox1.SelectedIndex]);
                temp.ShowDialog();
                if (temp.dr == DialogResult.OK)
                {
                    StoneFactory factory = new StoneFactory();
                    AbstractStone newNecklace = factory.CreateStone(temp.ClassName, temp.NameForm2, temp.WeightForm2, temp.CostForm2);
                    necklace.NecklaceItself[listBox1.SelectedIndex] = newNecklace;
                    forCalc();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 temp = new Form2();
            temp.ShowDialog();
            if (temp.dr == DialogResult.OK)
            {
                StoneFactory factory = new StoneFactory();
                AbstractStone newStone = factory.CreateStone(temp.ClassName, temp.NameForm2, temp.WeightForm2, temp.CostForm2);
                necklace.AddToNecklace(newStone);
                forCalc();
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
