﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Enities;
using Logic;

namespace UI1
{
    public partial class Form2 : Form
    {
        public string ClassName;
        public string NameForm2;
        public int CostForm2;
        public double WeightForm2;
        public DialogResult dr;

        public Form2()
        {
            InitializeComponent();

            comboBox1.Text = "Выберите класс";
            comboBox1.Items.Add("Diamond");
            comboBox1.Items.Add("Emerald");
            comboBox1.Items.Add("Ruby");
            comboBox1.Items.Add("Aquamarine");
            comboBox1.Items.Add("Moonstone");
            comboBox1.Items.Add("Tourmaline");
        }

        public Form2(AbstractStone stone):this()
        {
            textBox1.Text = stone.Name;
            textBox2.Text = stone.Cost.ToString();
            textBox3.Text = stone.Weight.ToString();
            comboBox1.SelectedItem = stone.checkClass();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "") || (textBox2.Text == "") || (textBox3.Text ==
            "") || (comboBox1.SelectedItem == null))
            {
                label5.Text = "You should fill every gap";
            }
            else
            {
                label5.Text = "";
                NameForm2 = textBox1.Text;
                CostForm2 = Convert.ToInt32(textBox2.Text);
                WeightForm2 = Convert.ToDouble(textBox3.Text);
                ClassName = comboBox1.SelectedItem.ToString();
                dr = DialogResult.OK;
                Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dr = DialogResult.Cancel;
            Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
