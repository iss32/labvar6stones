﻿using System;
using Enities;

namespace Logic
{
    public class StoneFactory
    {
        public AbstractStone CreateStone(string className, string name, double weight, int cost)
        {
            AbstractStone stone = className switch
            {
                "Diamond" => new Diamond(),
                "Emerald" => new Emerald(),
                "Ruby" => new Ruby(),
                "Moonstone" => new Moonstone(),
                "Aquamarine" => new Aquamarine(),
                _ => new Tourmaline()
            };
            stone.Name = name;
            stone.Weight = weight;
            stone.Cost = cost;
            return stone;
        }

        public Necklace CreateNecklace()
        {
            Necklace necklace = new Necklace();

            Diamond diamond = new Diamond();
            diamond.Name = "Прекрасный брилиант";
            diamond.Cost = 100000;
            diamond.Transparency = 0.123;
            diamond.Weight = 10;
            diamond.Colour = "White";

            Emerald emerald = new Emerald();
            emerald.Name = "Изумруд пещерный";
            emerald.Cost = 5000;
            emerald.FacetsAmount = 64;
            emerald.Transparency = 0.234;
            emerald.Weight = 5;

            Ruby ruby = new Ruby();
            ruby.Weight = 6;
            ruby.Transparency = 0.345;
            ruby.Name = "Великолепный изумруд";
            ruby.Hardness = 1.85;
            ruby.Cost = 9000;

            Aquamarine aquamarine = new Aquamarine();
            aquamarine.Cost = 3000;
            aquamarine.Density = 56;
            aquamarine.Name = "Аквамарин поделочный";
            aquamarine.Saturation = 0.596;
            aquamarine.Weight = 100;

            Moonstone moonstone = new Moonstone();
            moonstone.Cost = 50000;
            moonstone.Name = "Огромный кусок лунного камня";
            moonstone.Saturation = 2.9;
            moonstone.Shine = 93.7;
            moonstone.Weight = 159;

            Tourmaline tourmaline = new Tourmaline();
            tourmaline.Cost = 9700;
            tourmaline.Name = "Целебный турмалин";
            tourmaline.Saturation = 3.7;
            tourmaline.Shape = "Round";
            tourmaline.Weight = 80;

            necklace.AddToNecklace(diamond);
            necklace.AddToNecklace(ruby);
            necklace.AddToNecklace(emerald);
            necklace.AddToNecklace(aquamarine);
            necklace.AddToNecklace(tourmaline);
            necklace.AddToNecklace(moonstone);

            return necklace;
        }
    }

    public class PriceCalculator
    {
        public double GetTotalPrice(Necklace necklace)
        {
            double totalPrice = 0;
            foreach (AbstractStone stone in necklace.NecklaceItself)
            {
                totalPrice += stone.Cost;
            }
            return totalPrice;
        }
    }

    public class WeightCalculator
    {
        public double GetTotalWeight(Necklace necklace)
        {
            double totalWeight = 0;
            foreach (AbstractStone stone in necklace.NecklaceItself)
            {
                totalWeight += stone.Weight;
            }
            return totalWeight;
        }
    }

    public class NecklacePrinter
    {
        public void PrintStone(AbstractStone stone)
        {
            Console.Write("\tНаименование: " + stone.Name + ", стоимость: " +
(stone.Cost).ToString() + ", вес (в каратах): " + (stone.Weight).ToString() + ";\n");
        }

        public void PrintNecklace(Necklace necklace)
        {
            Console.Write("Ожерелье: \n");
            foreach (AbstractStone stone in necklace.NecklaceItself)
            {
                PrintStone(stone);
            }
            PriceCalculator calculatorPrice = new PriceCalculator();
            WeightCalculator calculatorWeight = new WeightCalculator();
            Console.Write("Общая стоимость ожерелья: " +
           calculatorPrice.GetTotalPrice(necklace).ToString() +
           ", общий вес (в каратах): " + calculatorWeight.GetTotalWeight(necklace));
        }

    }
}